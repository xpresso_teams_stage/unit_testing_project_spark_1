
__all__ = ["AbstractPipelineComponent"]
__author__ = "KK"

import sys
import os
import time
import threading

from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    PipelineComponentException
from xpresso.ai.core.commons.utils.generic_utils import str2bool
from xpresso.ai.core.logging.xpr_log import XprLogger

from xpresso.ai.core.data.pipeline.interface_pipeline_component import \
    InterfacePipelineComponent
from xpresso.ai.core.data.pipeline.RunFieldValue import \
    RunFieldValue
from xpresso.ai.core.data.pipeline.pipeline_component_controller import \
    PipelineController
from xpresso.ai.core.commons.utils.constants import ENABLE_LOCAL_EXECUTION
from xpresso.ai.core.data.pipeline.report_status import StatusReporter


class AbstractPipelineComponent(InterfacePipelineComponent):

    def __init__(self, name=None):

        if name is None:
            raise PipelineComponentException()

        super().__init__()

        self.logger = XprLogger()

        # set run name
        self.xpresso_run_name = None

        # set component name
        if "PROJECT_NAME" in os.environ:
            self.name = os.environ["PROJECT_NAME"]
        else:
            self.name = name

        # set run_status
        self.run_status = RunFieldValue.RUN_STATUS_IDLE.value

        # set component_status
        self.component_status = RunFieldValue.RUN_STATUS_IDLE.value

        # state of component (to be saved on pause, and loaded on restart)
        self.state = None

        # status of component (to be reported on periodic basis - consists of status dict and metrics dict)
        self.status = None

        # output of component (to be stored on disk on completion)
        self.output = None

        # final results of component (to be stored in database on completion)
        self.results = None

        self.run_status_thread = None

        self.should_thread_continue = True

        self.status_reporter = None

        if str2bool(os.environ.get(ENABLE_LOCAL_EXECUTION, "False")):
            self.local_execution = True

        else:
            self.controller = PipelineController()
            self.local_execution = False

        self.OUTPUT_DIR = PipelineController.OUTPUT_DIR

    def report_status(self, status):
        """ Report status to the controller """
        self.status_reporter.report_pipeline_status(status=status)

    def start(self, xpresso_run_name):

        """
        start the experiment corresponding to the xpresso_run_name
        Args:
            xpresso_run_name (str) : Unique identifier of that run instance
        """

        print("Parent component starting", flush=True)
        self.xpresso_run_name = xpresso_run_name

        if self.status_reporter is None:
            self.status_reporter = StatusReporter(component_name=self.name,
                                                  xpresso_run_name=self.xpresso_run_name)

        if self.local_execution:
            return
        # check status immediately and take action if required
        try:
            self.get_run_status()
        except Exception as e:
            print("Failed to check the status: {}".format(str(e)), flush=True)

        # start thread to check run status
        self.run_status_thread = threading.Thread(target=self.check_run_status)
        self.run_status_thread.start()
        self.should_thread_continue = True
        try:
            self.controller.pipeline_component_started(self.xpresso_run_name,
                                                       self.name)
        except Exception as e:
            print("Failed to send start status: {}".format(str(e)), flush=True)

    def terminate(self):
        print("Parent component terminating", flush=True)
        if self.local_execution:
            return

        self.should_thread_continue = False
        try:
            self.controller.pipeline_component_terminated(self.xpresso_run_name,
                                                          self.name)
        except Exception as e:
            print("Failed to send terminate status: {}".format(str(e)), flush=True)
        finally:
            sys.exit(0)

    def pause(self, push_exp=False):

        print("Parent component saving state and exiting", flush=True)
        if self.local_execution:
            return

        self.should_thread_continue = False
        try:
            self.controller.pipeline_component_paused(self.xpresso_run_name,
                                                      self.name,
                                                      self.state,
                                                      push_exp=push_exp)
        except Exception as e:
            print("Failed to send pause status: {}".format(str(e)), flush=True)
        finally:
            sys.exit(0)

    def restart(self):
        print("Parent component restarting", flush=True)
        if self.local_execution:
            return

        state = None
        try:
            state = self.controller.pipeline_component_restarted(
                self.xpresso_run_name,
                self.name)
        except Exception as e:
            print("Failed to send restart status: {}".format(str(e)), flush=True)

        if state is not None:
            self.state = state
            try:
                self.controller.update_field(self.xpresso_run_name,
                                             field_value=RunFieldValue.RUN_STATUS_RUNNING.value)
            except Exception as e:
                print("Failed to update RUNNING status: {}".format(str(e)), flush=True)

        else:
            print(
                "This component already completed execution. Some other "
                "component needs to restart. Terminating", flush=True)
            self.terminate()

    def completed(self, push_exp=True):
        print("Parent component completed", flush=True)
        if self.local_execution:
            return

        self.should_thread_continue = False
        try:
            self.controller.pipeline_component_completed(self.xpresso_run_name,
                                                         self.name, self.results,
                                                         push_exp=push_exp)
        except Exception as e:
            print("Failed to send completed status: {}".format(str(e)), flush=True)
        finally:
            sys.exit(0)

    def exit_thread(self):
        """
        called when execution of the component is completed
        Args:
            push_exp: flag to specify if exp info has to be pushed or not
        """
        print("Parent component exiting", flush=True)
        if self.local_execution:
            return

        self.should_thread_continue = False
        sys.exit(0)

    def check_run_status(self):
        while self.should_thread_continue:
            try:
                self.get_run_status()
            except Exception as e:
                print("Failed to check the status: {}".format(str(e)), flush=True)
            time.sleep(5)
        print("Stopping the check run status thread", flush=True)

    def get_run_status(self):
        self.run_status, self.component_status = \
            self.controller.get_pipeline_run_status(
                self.xpresso_run_name, self.name)
        self.logger.info(f"RUN STATUS: {self.run_status}\n "
                         f"COMPONENT STATUS: {self.component_status}")
        if self.run_status == RunFieldValue.RUN_STATUS_IDLE.value:
            pass
        elif self.run_status == RunFieldValue.RUN_STATUS_RUNNING.value:
            pass
        elif self.run_status == RunFieldValue.RUN_STATUS_TERMINATE.value \
                or self.run_status == 'TERMINATE':
            # Adding the 'or' case for backward compatibility
            self.logger.info("Terminating component")
            self.terminate()
        elif self.run_status == RunFieldValue.RUN_STATUS_PAUSED.value:
            self.logger.info("Pausing component")
            self.pause()
        elif self.run_status == RunFieldValue.RUN_STATUS_RESTART.value:
            self.logger.info("Restarting component")
            self.restart()
        elif self.run_status == RunFieldValue.RUN_STATUS_COMPLETED.value:
            self.logger.info("Completing component")
            self.controller.pipeline_component_completed(self.xpresso_run_name,
                                                         self.name,
                                                         self.results,
                                                         push_exp=False)
            self.exit_thread()
        else:
            self.logger.info("No action required...continuing")

        if self.component_status == RunFieldValue.RUN_STATUS_COMPLETED.value:
            self.logger.info("Completing component")
            self.exit_thread()
